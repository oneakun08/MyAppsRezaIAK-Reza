package com.harjayreza.myappsreza_iak;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Skaha_AM on 16-Feb-18.
 */

public class ResultActivity extends AppCompatActivity {
    private TextView tvNama, tvAlamat, tvNo, tvEmail, tvTahun;
    private String getNama, getAlamat, getNo, getEmail, getTahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        initUI();
        setBundle();
        setView();
        Button bNext;
        bNext = findViewById(R.id.bNext);

        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResultActivity.this, MainContent.class);
                startActivity(intent);
            }
        });
    }

    private void setView() {
        tvNama.setText("Selamat Datang " + getNama + ", Anda Berhasil Registrasi");
        tvTahun.setText("Tahun : " + getTahun);
        tvAlamat.setText("Alamat : " + getAlamat);
        tvNo.setText("No. Telp : " + getNo);
        tvEmail.setText("Email : " + getEmail);
    }

    private void initUI() {
        tvNama = findViewById(R.id.view_nama);
        tvAlamat = findViewById(R.id.view_alamat);
        tvNo = findViewById(R.id.view_no);
        tvEmail = findViewById(R.id.view_email);
        tvTahun = findViewById(R.id.view_tahun);
    }

    private void setBundle() {
        Bundle b = getIntent().getExtras();
        getNama = b.getString("Nama");
        getAlamat = b.getString("Alamat");
        getTahun = b.getString("Tahun");
        getNo = b.getString("No");
        getEmail = b.getString("Email");
    }
}

