package com.harjayreza.myappsreza_iak;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Skaha_AM on 16-Feb-18.
 */

public class InputActivity extends AppCompatActivity {
    private EditText tvNama, tvAlamat, tvNo, tvEmail, tvTahun;
    private Button btnSubmit, btnHapus;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        initUI();
        Hapus();
        Submit();
    }

    private void Submit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean valid = true;
                String nama = tvNama.getText().toString();
                String tahun = tvTahun.getText().toString();
                String alamat = tvAlamat.getText().toString();
                String no = tvNo.getText().toString();
                String email = tvEmail.getText().toString().trim();

                if (nama.isEmpty()) {
                    tvNama.setError("Isikan Nama");
                    valid = false;
                }

                if (alamat.isEmpty()) {
                    tvAlamat.setError("Isikan Alamat");
                    valid = false;
                }

                if (tahun.isEmpty()) {
                    tvTahun.setError("Isikan Tahun");
                    valid = false;
                } else if (tahun.length() != 4) {
                    tvAlamat.setError("Isikan Tahun dengan benar");
                    valid = false;
                }

                if (no.isEmpty()) {
                    tvNo.setError("Isikan No Telp");
                    valid = false;
                } else if (no.length() < 9 || no.length() > 13) {
                    tvNo.setError("Isikan No. Telp dengan benar");
                    valid = false;
                }

                if (email.isEmpty()) {
                    tvEmail.setError("Isikan Email");
                    valid = false;
                }

                if (valid == true) {
                    Toast.makeText(InputActivity.this, "Login Success.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(InputActivity.this, ResultActivity.class);
                    Bundle b = new Bundle();
                    b.putString("Nama", nama);
                    b.putString("Alamat", alamat);
                    b.putString("Tahun", tahun);
                    b.putString("No", no);
                    b.putString("Email", email);
                    intent.putExtras(b);
                    startActivity(intent);
                }
            }
        });
    }

    private void Hapus() {
        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAlamat.setText("");
                tvNama.setText("");
                tvNo.setText("");
                tvEmail.setText("");
                tvTahun.setText("");
            }
        });
    }

    private void initUI() {
        tvNama = findViewById(R.id.tv_nama);
        tvAlamat = findViewById(R.id.tv_alamat);
        tvNo = findViewById(R.id.tv_no);
        tvEmail = findViewById(R.id.tv_email);
        tvTahun = findViewById(R.id.tv_tahun);
        btnSubmit = findViewById(R.id.submit);
        btnHapus = findViewById(R.id.hapus);
    }
}

