package com.harjayreza.myappsreza_iak;

/**
 * Created by Skaha_AM on 16-Feb-18.
 */

public class MovieModel {
    private String title;
    private String released;
    private String genre;

    public MovieModel() {
    }

    public MovieModel(String title, String released, String genre) {
        this.title = title;
        this.released = released;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public String getReleased() {
        return released;
    }

    public String getGenre() {
        return genre;
    }

}
